#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>

#include "md5.h"
#include "binsearch.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *h = md5(guess, strlen(guess));
    
    // Compare the two hashes
    if (strcmp(hash, h) == 0) 
    {
        return 1;
    }
    return 0;
    
    // Free any malloc'd memory
    free(h);
}

// TODO
// Read in the hash file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **read_hashes(char *filename)
{
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        printf("Couldn't open %s.\n", filename);
        exit(1);
    }
    int size = 0;
    char **m = (char **)malloc(size*sizeof(char*));
    char temp[100];
    int i = 0;
    while(fscanf(f, "%s", temp) != EOF)
    {
        if(i == size)
        {
            size +=100;
            char** newm = (char**)realloc(m, size*sizeof(char*));
            if(newm == NULL)
            {
                puts("Couldn't extend the array.\n");
                exit(1);
            }
            else
                m = newm;
        }
        int len = strlen(temp);
        m[i] = (char *)malloc((len+1)*sizeof(char*));
        strcpy(m[i], temp);
        i++;
    }
    fclose(f);
    m[i]=NULL;
    return m;
}


// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.
char **read_dict(char *filename)
{
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        printf("Couldn't open %s.\n", filename);
        exit(1);
    }
        int size = 0;
        char **m = (char **)malloc(size*sizeof(char*));
        char password[PASS_LEN];
        char *pass = NULL;
        int i = 0;
        while(fscanf(f, "%s", password) != EOF)
        {
            pass = md5(password, strlen(password));
            if(i == size)
            {
                size +=100;
                char** newm = (char**)realloc(m, size*sizeof(char*));
                if(newm == NULL)
                {
                    puts("Couldn't extend the array.\n");
                    exit(1);
                }
                else
                    m = newm;
            }
            int len = strlen(password);
            m[i] = (char *)malloc((len+37)*sizeof(char));
            strcpy(m[i], pass);
            strcat(strcat(m[i], " = "), password);
            i++;
        }
        m[i]=NULL;
        fclose(f);
        return m;
}

int compare(const void *a, const void *b) //the function is not allowed to changed the value of a or b
{
    return strcmp(*(char **)a, *(char **)b);
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // TODO: Read the dictionary file into an array of strings
    printf("Reading dictionary\n");
    char **dict = read_dict(argv[2]);
    printf("Done\n");
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    int count = 0;  
    char c;  
    FILE *fp = fopen(argv[2], "r");
    for (c = getc(fp); c != EOF; c = getc(fp))
    {
        if (c == '\n') 
            count = count + 1;
    }
    fclose(fp);
    qsort(dict, count, sizeof(char *), compare);
    char *s;
    int i = 0;
    while(hashes[i] !=NULL)
    {
        s = binsearch(dict, count, hashes[i]);
        printf("%d %s\n", i+1, s);
        i++;
    }
}