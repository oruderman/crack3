// binsearch
// Given:
//   pointer to array of char *
//   length of the array
//   target string
// Returns:
//   string containing the target
#include <string.h>
char *binsearch(char **arr,  int len, char *target)
{

   int first = 0;
   int last = len -1;
   int middle = (first+last)/2;
   while( first <= last )
   {
        if (strncmp(arr[middle], target, 32) < 0 )
        {
            first = middle + 1;
        }
        else if (strncmp(arr[middle], target, 32) == 0)
        {
            return arr[middle];
        }
        else
            last = middle - 1;
            middle = (first + last)/2;
    }
    return arr[middle];
}